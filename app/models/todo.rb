class Todo < ActiveRecord::Base
  def url
     "http://#{ENV['HOST_IP']}:#{ENV["HOST_PORT"]}/#{id}"
  end

  def serializable_hash config
    super(config).merge(url: url)
  end
end
